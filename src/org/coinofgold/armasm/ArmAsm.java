package org.coinofgold.armasm;

public class ArmAsm {
	
	// A reference to the UI
	UI ui;
	
	public static void main(String[] args) {
		new ArmAsm();
	}
	
	public ArmAsm() {
		// Create the UI
		ui = new UI();
		// Set the location to the middle of the screen
		ui.setLocationRelativeTo(null);
		// Display the window
		ui.setVisible(true);
	}

}
