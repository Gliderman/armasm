package org.coinofgold.armasm;

import java.util.*;

public class Assembler {

	// Arithmetic
	static final Instruction ADD = new Instruction("ADD", "10001011000", 4);
	static final Instruction SUB = new Instruction("SUB", "11001011000", 4);
	static final Instruction ADDI = new Instruction("ADDI", "1001000100", 4);
	static final Instruction SUBI = new Instruction("SUBI", "1101000100", 4);
	static final Instruction ADDS = new Instruction("ADDS", "10101011000", 4);
	static final Instruction SUBS = new Instruction("SUBS", "11101011000", 4);
	static final Instruction ADDIS = new Instruction("ADDIS", "1011000100", 4);
	static final Instruction SUBIS = new Instruction("SUBIS", "1111000100", 4);
	// Data transfer
	static final Instruction STUR = new Instruction("STUR", "11111000000", 4);
	static final Instruction LDUR = new Instruction("LDUR", "11111000010", 4);
	static final Instruction STURB = new Instruction("STURB", "00111000000", 4);
	static final Instruction LDURB = new Instruction("LDURB", "00111000010", 4);
	static final Instruction MOVZ = new Instruction("MOVZ", "110100101", 3);
	static final Instruction MOVK = new Instruction("MOVK", "111100101", 3);
	// Logic
	static final Instruction AND = new Instruction("AND", "10001010000", 4);
	static final Instruction ORR = new Instruction("ORR", "10101010000", 4);
	static final Instruction EOR = new Instruction("EOR", "11001010000", 4);
	static final Instruction ANDI = new Instruction("ANDI", "1001001000", 4);
	static final Instruction ORRI = new Instruction("ORRI", "1011001000", 4);
	static final Instruction EORI = new Instruction("EORI", "1101001000", 4);
	static final Instruction ANDS = new Instruction("ANDS", "11101010000", 4);
	static final Instruction ANDIS = new Instruction("ANDIS", "1111001000", 4);
	static final Instruction LSR = new Instruction("LSR", "11010011010", 4);
	static final Instruction LSL = new Instruction("LSL", "11010011011", 4);
	// Conditional branch
	static final Instruction CBZ = new Instruction("CBZ", "10110100", 3);
	static final Instruction CBNZ = new Instruction("CBNZ", "10110101", 3);
	static final Instruction BCOND = new Instruction("B.", "01010100", 2); // Special case that will use startsWith() instead of equals()
	// Unconditional jump
	static final Instruction B = new Instruction("B", "000101", 2);
	static final Instruction BR = new Instruction("BR", "11010110000", 2);
	static final Instruction BL = new Instruction("BL", "100101", 2);
	// Condition
	static final Instruction EQ = new Instruction("EQ", "00000", 0);
	static final Instruction NE = new Instruction("NE", "00001", 0);
	static final Instruction HS = new Instruction("HS", "00010", 0);
	static final Instruction LO = new Instruction("LO", "00011", 0);
	static final Instruction MI = new Instruction("MI", "00100", 0);
	static final Instruction PL = new Instruction("PL", "00101", 0);
	static final Instruction VS = new Instruction("VS", "00110", 0);
	static final Instruction VC = new Instruction("VC", "00111", 0);
	static final Instruction HI = new Instruction("HI", "01000", 0);
	static final Instruction LS = new Instruction("LS", "01001", 0);
	static final Instruction GE = new Instruction("GE", "01010", 0);
	static final Instruction LT = new Instruction("LT", "01011", 0);
	static final Instruction GT = new Instruction("GT", "01100", 0);
	static final Instruction LE = new Instruction("LE", "01101", 0);
	static final Instruction AL = new Instruction("AL", "01110", 0);
	static final Instruction NV = new Instruction("NV", "01111", 0);
	
	// Parameters
	static final int MAX_REG = 32;
	
	// Errors
	static final String INVALID_COMMAND = "INVALID COMMAND";
	static final String INVALID_REGISTER = "INVALID REGISTER";
	static final String INVALID_NUMBER = "INVALID NUMBER";
	static final String INVALID_CONDITION = "INVALID CONDITION";
	static final String WRONG_PARAMETER_COUNT = "WRONG PARAMETER COUNT";
	static final String DUPLICATE_LABEL = "DUPLICATE LABEL";
	
	// Label tracking
	static Map<String, Integer> labels = new HashMap<String, Integer>();
	
	public static void resetLabels() {
		labels = new HashMap<String, Integer>();
	}

	public static String assemble(String in, int line, boolean scanMode) {
		String out = null;
		// Clean up input from user styling
		String cleaned = in.trim().toUpperCase();
		cleaned = cleaned.replace(",", "");
		cleaned = cleaned.replace("[", "");
		cleaned = cleaned.replace("]", "");
		cleaned = cleaned.replace(":", "");
		String[] lineParts = cleaned.split(" ");
		
		if (lineParts.length == 1) { // Label check
			if (scanMode) {
				if (labels.containsKey(lineParts[0])) {
					out = DUPLICATE_LABEL + ": " + lineParts[0] + " on " + labels.get(lineParts[0]) + " and " + line;
				} else {
					labels.put(lineParts[0], line);
					out = "LABEL";
				}
			} else {
				return "//"; // Triggers displaying it as a comment
			}
		}
		if (scanMode) {
			return out;
		}
		
		if (lineParts.length >= 2) { // Regular parts
			if (lineParts[0].equals(ADD.inst)) { // ***** ARITHMETIC *****
				if (lineParts.length < ADD.numParts) return WRONG_PARAMETER_COUNT;
				out = ADD.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(SUB.inst)) {
				if (lineParts.length < SUB.numParts) return WRONG_PARAMETER_COUNT;
				out = SUB.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ADDI.inst)) {
				if (lineParts.length < ADDI.numParts) return WRONG_PARAMETER_COUNT;
				out = ADDI.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(SUBI.inst)) {
				if (lineParts.length < SUBI.numParts) return WRONG_PARAMETER_COUNT;
				out = SUBI.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ADDS.inst)) {
				if (lineParts.length < ADDS.numParts) return WRONG_PARAMETER_COUNT;
				out = ADDS.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(SUBS.inst)) {
				if (lineParts.length < SUBS.numParts) return WRONG_PARAMETER_COUNT;
				out = SUBS.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ADDIS.inst)) {
				if (lineParts.length < ADDIS.numParts) return WRONG_PARAMETER_COUNT;
				out = ADDIS.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(SUBIS.inst)) {
				if (lineParts.length < SUBIS.numParts) return WRONG_PARAMETER_COUNT;
				out = SUBIS.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
				
				
			} else if (lineParts[0].equals(STUR.inst)) { // ***** DATA TRANSFER *****
				if (lineParts.length < STUR.numParts) return WRONG_PARAMETER_COUNT;
				out = STUR.binary + intToBinary(lineParts[3], 9, line) + "00" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(LDUR.inst)) {
				if (lineParts.length < LDUR.numParts) return WRONG_PARAMETER_COUNT;
				out = LDUR.binary + intToBinary(lineParts[3], 9, line) + "00" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(STURB.inst)) {
				if (lineParts.length < STURB.numParts) return WRONG_PARAMETER_COUNT;
				out = STURB.binary + intToBinary(lineParts[3], 9, line) + "00" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(LDURB.inst)) {
				if (lineParts.length < LDURB.numParts) return WRONG_PARAMETER_COUNT;
				out = LDURB.binary + intToBinary(lineParts[3], 9, line) + "00" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(MOVZ.inst)) {
				if (lineParts.length < MOVZ.numParts) return WRONG_PARAMETER_COUNT;
				out = MOVZ.binary + "00" + intToBinary(lineParts[2], 16, line) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(MOVK.inst)) {
				if (lineParts.length < MOVK.numParts) return WRONG_PARAMETER_COUNT;
				out = MOVK.binary + "00" + intToBinary(lineParts[2], 16, line) + getRegister(lineParts[1]);
				
				
			} else if (lineParts[0].equals(AND.inst)) { // ***** LOGIC *****
				if (lineParts.length < AND.numParts) return WRONG_PARAMETER_COUNT;
				out = AND.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ORR.inst)) {
				if (lineParts.length < ORR.numParts) return WRONG_PARAMETER_COUNT;
				out = ORR.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(EOR.inst)) {
				if (lineParts.length < EOR.numParts) return WRONG_PARAMETER_COUNT;
				out = EOR.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ANDI.inst)) {
				if (lineParts.length < ANDI.numParts) return WRONG_PARAMETER_COUNT;
				out = ANDI.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ORRI.inst)) {
				if (lineParts.length < ORRI.numParts) return WRONG_PARAMETER_COUNT;
				out = ORRI.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(EORI.inst)) {
				if (lineParts.length < EORI.numParts) return WRONG_PARAMETER_COUNT;
				out = EORI.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ANDS.inst)) {
				if (lineParts.length < ANDS.numParts) return WRONG_PARAMETER_COUNT;
				out = ANDS.binary + getRegister(lineParts[3]) + "000000" + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(ANDIS.inst)) {
				if (lineParts.length < ANDIS.numParts) return WRONG_PARAMETER_COUNT;
				out = ANDIS.binary + intToBinary(lineParts[3], 12, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(LSR.inst)) {
				if (lineParts.length < LSR.numParts) return WRONG_PARAMETER_COUNT;
				out = LSR.binary + "00000" + intToBinary(lineParts[3], 6, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(LSL.inst)) {
				if (lineParts.length < LSL.numParts) return WRONG_PARAMETER_COUNT;
				out = LSL.binary + "00000" + intToBinary(lineParts[3], 6, line) + getRegister(lineParts[2]) + getRegister(lineParts[1]);
				
				
			} else if (lineParts[0].equals(CBZ.inst)) { // ***** CONDITIONAL BRANCH *****
				if (lineParts.length < CBZ.numParts) return WRONG_PARAMETER_COUNT;
				out = CBZ.binary + intToBinary(lineParts[2], 19, line) + getRegister(lineParts[1]);
			} else if (lineParts[0].equals(CBNZ.inst)) {
				if (lineParts.length < CBNZ.numParts) return WRONG_PARAMETER_COUNT;
				out = CBNZ.binary + intToBinary(lineParts[2], 19, line) + getRegister(lineParts[1]);
			} else if (lineParts[0].startsWith(BCOND.inst)) { // Use startsWith() instead
				if (lineParts.length < BCOND.numParts) return WRONG_PARAMETER_COUNT;
				if (lineParts[0].length() == 4) {
					String cond = lineParts[0].substring(2);
					if (cond.equals(EQ.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + EQ.binary;
					} else if (cond.equals(NE.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + NE.binary;
					} else if (cond.equals(HS.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + HS.binary;
					} else if (cond.equals(LO.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + LO.binary;
					} else if (cond.equals(MI.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + MI.binary;
					} else if (cond.equals(PL.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + PL.binary;
					} else if (cond.equals(VS.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + VS.binary;
					} else if (cond.equals(VC.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + VC.binary;
					} else if (cond.equals(HI.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + HI.binary;
					} else if (cond.equals(LS.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + LS.binary;
					} else if (cond.equals(GE.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + GE.binary;
					} else if (cond.equals(LT.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + LT.binary;
					} else if (cond.equals(GT.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + GT.binary;
					} else if (cond.equals(LE.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + LE.binary;
					} else if (cond.equals(AL.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + AL.binary;
					} else if (cond.equals(NV.inst)) {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + NV.binary;
					} else {
						out = BCOND.binary + intToBinary(lineParts[1], 19, line) + INVALID_CONDITION;
					}
				} else {
					out = BCOND.binary + intToBinary(lineParts[1], 19, line) + INVALID_CONDITION;
				}
				
				
			} else if (lineParts[0].equals(B.inst)) { // ***** UNCONDITIONAL JUMP *****
				if (lineParts.length < B.numParts) return WRONG_PARAMETER_COUNT;
				out = B.binary + intToBinary(lineParts[1], 26, line);
			} else if (lineParts[0].equals(BR.inst)) {
				if (lineParts.length < BR.numParts) return WRONG_PARAMETER_COUNT;
				out = BR.binary + "00000000000" + getRegister(lineParts[1]) + "00000";
			} else if (lineParts[0].equals(BL.inst)) {
				if (lineParts.length < BL.numParts) return WRONG_PARAMETER_COUNT;
				out = BL.binary + intToBinary(lineParts[1], 26, line);
				
				
			} else {
				out = INVALID_COMMAND;
			}
		} else {
			out = WRONG_PARAMETER_COUNT;
		}

		return out;
	}
	
	public static String intToBinary(String intIn, int width, int line) {
		try {
			// Convert to integer then binary
			int toInt = Integer.parseInt(intIn);
			String nowBinary = Integer.toBinaryString(toInt);
			
			// Cut off some of the 32 bits when using a negative number
			if (nowBinary.length() > width) {
				// Cut off the most significant digits
				nowBinary = nowBinary.substring(32 - width);
			}
			
			// Zero pad by cutting off the existing length and attaching the remaining zeros
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < width; i++) {
				sb.append("0");
			}
			String zeroPad = sb.toString();
			
			return zeroPad.substring(nowBinary.length()) + nowBinary;
		} catch (Exception e) {
			if (labels.containsKey(intIn)) {
				try {
					int newDifference = labels.get(intIn) - line - 1;
					return intToBinary("" + newDifference, width, -1);
				} catch (Exception ee) {
					// Ignore
				}
				return INVALID_NUMBER;
			} else {
				return INVALID_NUMBER;
			}
		}
	}
	
	public static String getRegister(String regString) {
		// Make sure there is some data
		if (regString.length() <= 1) {
			return INVALID_REGISTER;
		}
		
		// Make sure it starts with either an R or an X
		if (!regString.startsWith("R") && !regString.startsWith("X")) {
			return INVALID_REGISTER;
		}
		
		// Handle shortcut for zero register
		if ((regString.equals("XZR")) || (regString.equals("RZR"))) {
			return "11111";
		}
		
		// Convert to integer
		String regNumString = regString.substring(1);
		int regNum = 0;
		try {
			regNum = Integer.parseInt(regNumString);
		} catch (NumberFormatException nfe) {
			return INVALID_REGISTER;
		}
		String zeroPad = "00000";
		
		// Make sure the number is within the bounds
		if (regNum >= MAX_REG) {
			return INVALID_REGISTER;
		} else {
			// Convert to binary
			String reg = Integer.toBinaryString(regNum);
			// Zero pad by cutting off the existing length and attaching the remaining zeros
			return zeroPad.substring(reg.length()) + reg;
		}
	}
	
}
