# ArmAsm

Quick assembler for LEGv8.

![ArmAsm Screenshot](https://bitbucket.org/Gliderman/armasm/raw/8ed93f3b2823ab766f1bd139ffdb99e586396a4d/ArmAsm.PNG)

## Download

Download a ready-to-run version from [Downloads](https://bitbucket.org/Gliderman/armasm/downloads/). It should run on Java 6 and up.

## Developers

- Nate Hoffman
- Zachary Smoot
